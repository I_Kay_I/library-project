import { Controller, Get, Post, Body } from '@nestjs/common';
import { UsersService } from './users.service';
import { UsersModel } from './model/users.model';

@Controller('users')
export class UsersController {
    constructor(
        private usersService: UsersService
    ) {}

    @Post("paging")
    public paging(@Body() page): Promise<any> {
        return this.usersService.paging(page.currentPage)
    }

    @Get("getTotal") 
    public getTotal(): Promise<{}> {
        return this.usersService.getTotal()
    }

    @Post("deleteUser")
    public async deleteUser(@Body() user: UsersModel): Promise<UsersModel[]> {
        console.log("user")
        return this.usersService.deleteUser(user)
    }

    @Post("changeUser")
    public async changeUser(@Body() user: UsersModel): Promise<UsersModel> {
        console.log("user is changed")
        return this.usersService.changeUser(user)
    }
}
