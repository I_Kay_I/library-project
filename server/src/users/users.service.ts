import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { UsersModel } from './model/users.model';
import * as bcrypt from 'bcrypt';
import { InjectModel } from '@nestjs/mongoose';

@Injectable()
export class UsersService {
    constructor(
        @InjectModel("Users") private readonly usersModel: Model<UsersModel>
    ) {}

    public async getTotal(): Promise<{}> {
        const total = this.usersModel.find().count()
        return total
    }

    public async paging(page): Promise<any> {
        console.log(page)
        const perPage = 5;
        const start = (page - 1) * perPage;
        const usersOnPage = this.usersModel.find().skip(start).limit(perPage)
        return usersOnPage
    }

    public async deleteUser(user): Promise<UsersModel[]> {
        console.log("user is delete")
        return this.usersModel.remove({_id: user._id})
    }

    public async changeUser(user): Promise<UsersModel> {
        const saltRounds = 10
        let salt = bcrypt.genSaltSync(saltRounds);
        let hash = bcrypt.hashSync(user.userPassword, salt);
        user.userPassword = hash
        return this.usersModel.updateOne({_id: user._id}, user)
    }
}
