import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { MongooseModule } from '@nestjs/mongoose';
import { UsersSchema } from './users-schema/users.schema';
import { UsersController } from './users.controller';

@Module({
  imports: [
    MongooseModule.forFeature([
      {name: "Users", schema: UsersSchema}
    ])
  ],
  controllers: [UsersController],
  providers: [UsersService]
})
export class UsersModule {}
