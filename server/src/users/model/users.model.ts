export class UsersModel {
    id: string;
    userName: string;
    userEmail: string;
    userPassword: string;
    userRole: string;
    userGender: string;
}