import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { AuthModel } from './model/auth.model';
import { Model } from 'mongoose';
import * as bcrypt from 'bcrypt';
import { async } from 'rxjs/internal/scheduler/async';
import { JwtService } from '@nestjs/jwt';
import { JwtPayload } from 'src/strategy/model/jwt.model';


@Injectable()
export class AuthService {
    constructor(
        @InjectModel("Users") private readonly authModel: Model<AuthModel>,
        private readonly jwtService: JwtService
    ) {}

    public async create(user: AuthModel): Promise<AuthModel> {
        const saltRounds = 10
        let createdUser = new this.authModel(user)
        bcrypt.hash(createdUser.userPassword, saltRounds, async function (err, hash) {
            createdUser.userPassword = hash
            return await createdUser.save()
        });     
        return createdUser;
    }

    public async findUserByName(username: string): Promise<AuthModel> {
        const user = await this.authModel.findOne({userName: username});
        return user;
    }

    public async singIn(userName: string): Promise<string> {
        const neededUser = await this.findUserByName(userName)
        const user: JwtPayload = {
            _id: neededUser.id,
            userName: neededUser.userName,
            userEmail: neededUser.userEmail,
            userGender: neededUser.userGender,
            userRole: neededUser.userRole
        };
        return this.jwtService.sign(user)
    }

    // public validateUser(payload: any): Promise<any> {
    //     return this.findOneByEmail(payload.email)
    // }
}
