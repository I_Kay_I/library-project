export class AuthModel {
    id: string;
    userName: string;
    userPassword: string;
    userEmail: string;
    userGender: string;
    userRole: string;
}