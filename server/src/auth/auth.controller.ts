import { Controller, Body, Post, HttpException, HttpStatus } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthModel } from './model/auth.model';
import * as bcrypt from 'bcrypt';


@Controller('auth')
export class AuthController {
    constructor(
        private authService: AuthService
    ) {

    }

    @Post("createUser")
    public async createUser(@Body() user: AuthModel): Promise<AuthModel> {
        const checkUserByName = await this.authService.findUserByName(user.userName)
        if (checkUserByName) {
            throw new HttpException('This username is in use', HttpStatus.METHOD_NOT_ALLOWED);
        } 
        console.log("user is add")
        return this.authService.create(user)

    }

    @Post("login")
    public async login(@Body() body: { userName: string; userPassword: string}): Promise<{token: string}> {
        const neededUser =  await this.authService.findUserByName(body.userName);
        const token = await this.authService.singIn(body.userName);
        const match = await bcrypt.compare (body.userPassword, neededUser.userPassword)
        if (!neededUser) {
            throw new HttpException( 'Wrong username', HttpStatus.FORBIDDEN);
        }

        if (!match) {
            throw new HttpException( 'Wrong password', HttpStatus.FORBIDDEN);
        }
        
        if (neededUser && match) {
            console.log(token)
            return {token}
        }
    }
}
