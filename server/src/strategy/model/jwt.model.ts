export class JwtPayload {
    _id: String;
    userName: string;
    userEmail: string;
    userGender: string;
    userRole: string
}