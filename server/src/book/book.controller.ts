import { Controller, Get, Param, Post, Body } from '@nestjs/common';
import { BookModel } from './model/book.model';
import { BookService } from './book.service';

@Controller('books')
export class BookController {
    constructor(
        private bookService: BookService
    ) {

    }

    @Get("getBooks")
    public getAllBooks(): Promise<BookModel[]> {
        return this.bookService.getBooks();
    }

    @Get("getById/:id")
    public getById(@Param("id") id: string): Promise<BookModel> {
        return this.bookService.findBookById(id)
    }

    @Post("addNewBook")
    public addNewBook(@Body() book: BookModel): Promise<BookModel> {
        return this.bookService.addNewBook(book)
    }

    @Post("deleteBook")
    public deleteBook(@Body() book: BookModel): Promise<BookModel> {
        return this.bookService.deleteBook(book)
    }

    @Post("changeBook")
    public changeBook(@Body() book: BookModel): Promise<BookModel> {
        return this.bookService.changeBook(book)
    }

    @Post("searchByTitle") 
    public searchByTitle(@Body() title): Promise<BookModel[]> {
        return this.bookService.findBookByName(title.title)
    }

    @Post("searchByAuthor") 
    public searchByAuthor(@Body() author): Promise<BookModel[]> {
        return this.bookService.findBookByAuthor(author.author)
    }

    @Post("searchByType") 
    public searchByType(@Body() type): Promise<BookModel[]> {
        return this.bookService.findBookByType(type.type)
    }

    @Post("searchByPrice")
    public searchByPrice(@Body() price): Promise<BookModel[]> {
        return this.bookService.findBookByPrice(price)
    }

    @Post("paging")
    public paging(@Body() paging): Promise<BookModel[]> {
        return this.bookService.paging(paging.currentPage)
    }

    @Get("getTotal") 
    public getTotal(): Promise<{}> {
        return this.bookService.getTotal()
    }


}
