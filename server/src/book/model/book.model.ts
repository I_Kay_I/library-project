export class BookModel {
    name: string;
    authors: [];
    description: string;
    type: string;
    price: number;
}