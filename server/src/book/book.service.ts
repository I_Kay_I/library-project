import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { BookModel } from './model/book.model';
import { AuthorModel } from 'src/authors/model/authors.model';

@Injectable()
export class BookService {
    constructor(
        @InjectModel("Books") private readonly bookModel: Model<BookModel>,
    ) {

    }

    public async findBookById(id: string): Promise<BookModel> {
        return await this.bookModel.findOne({_id: id})
    }

    public async findBookByName(title: string): Promise<BookModel[]> {
        const bookByName = this.bookModel.find({name: title})
        return await bookByName
    }

    public async findBookByAuthor(author: string): Promise<BookModel[]> {
        const bookByAuthor = this.bookModel.find({authors: {$elemMatch : {name: author}}})
        return await bookByAuthor
    }

    public async findBookByType(type: string): Promise<BookModel[]> {
        const bookByType = this.bookModel.find({type: type})
        return await bookByType
    }

    public async findBookByPrice(price): Promise<BookModel[]> {
        const bookByPrice = this.bookModel.find({$and : [{price: {$gt : price.min}}, {price: {$lt : price.max}}]})
        return await bookByPrice
    }

    public async getBooks(): Promise<BookModel[]> {
        return await this.bookModel.find().exec();
    }

    public async getTotal(): Promise<{}> {
        const total = this.bookModel.find().count()
        return total
    }

    public async paging(page): Promise<BookModel[]> {
        const perPage = 5;
        const start = (page - 1) * perPage;
        const booksOnPage = this.bookModel.find().skip(start).limit(perPage)
        return booksOnPage
    }

    public async addNewBook(book: any): Promise<BookModel> {
        const isBook = await this.findBookByName(book.newBookTitle);
        if(isBook.length !== 0) {
            throw new HttpException( 'this book is already exists', HttpStatus.FORBIDDEN);
        }

        if(isBook.length === 0) {
            let createdBook: BookModel = {
                name: book.newBookTitle,
                authors: book.newBookAuthor,
                description: book.newBookInfo,
                type: book.newBookType,
                price: book.newBookPrice
            }

            let newBook = new this.bookModel(createdBook)
            return newBook.save()
        }
    }

    public async deleteBook(book): Promise<BookModel> {
        const isBook = this.findBookById(book._id)

        if (!isBook) {
            throw new HttpException( 'book undefined', HttpStatus.FORBIDDEN);
        }   

        if (isBook) {
            return this.bookModel.remove({_id: book._id})
        }
    }

    public async changeBook(book): Promise<BookModel> {
        const isBook = this.findBookById(book._id)

        if (!isBook) {
            console.log("no")
            throw new HttpException( 'book undefined', HttpStatus.FORBIDDEN);
        }

        if (isBook) {
            console.log("yes")
            return this.bookModel.updateOne({_id: book._id}, book)
        }
    }
}
