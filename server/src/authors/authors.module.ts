import { Module } from '@nestjs/common';
import { AuthorsController } from './authors.controller';
import { AuthorsService } from './authors.service';
import { MongooseModule } from '@nestjs/mongoose';
import { AuthorSchema } from './author-schema/authors.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      {name: "Authors", schema: AuthorSchema}
    ])
  ],
  controllers: [AuthorsController],
  providers: [AuthorsService]
})
export class AuthorsModule {}
