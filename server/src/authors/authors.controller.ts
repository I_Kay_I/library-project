import { Controller, Body, Post, Get } from '@nestjs/common';
import { AuthorsService } from './authors.service';
import { AuthorModel } from './model/authors.model';

@Controller('authors')
export class AuthorsController {
    constructor(
        private authorsService: AuthorsService
    ) {}

    @Post("addNewAuthor")
    public async addNewAuthor(@Body() authorName): Promise<AuthorModel> {
        console.log(typeof(authorName))
        return this.authorsService.addNewAuthor(authorName)
    }

    @Post("paging")
    public async paging(@Body() paging): Promise<AuthorModel[]> {
        return this.authorsService.paging(paging.currentPage)
    }

    @Get("getTotal")
    public async getTotal(): Promise<{}> {
        return this.authorsService.getTotal()
    }

    @Get("getAllAuthors")
    public async getAllAuthors(): Promise<AuthorModel[]> {
        return this.authorsService.getAllAuthors()
    }

    @Post("changeAuthorName")
    public async changeAuthorName(@Body() author: AuthorModel): Promise<AuthorModel> {
        return this.authorsService.changeAuthorName(author)
    }

    @Post("deleteAuthor")
    public async deleteAuthor(@Body() author: AuthorModel): Promise<AuthorModel> {
        return this.authorsService.deleteAuthor(author)
    }
}
