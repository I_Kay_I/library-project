import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { AuthorModel } from './model/authors.model';

@Injectable()
export class AuthorsService {
    constructor(
        @InjectModel("Authors") private authorModel: Model<AuthorModel>
    ) {

    }

    public async findAuthorByName(authorName: string): Promise<AuthorModel[]> {
        const author = this.authorModel.find({name: authorName})
        return await author
    }

    public async findAuthorById(authorId): Promise<AuthorModel> {
        const author = this.authorModel.findOne({_id: authorId})
        return await author
    }

    public async addNewAuthor(author): Promise<AuthorModel> {
        const isAuthor = await this.findAuthorByName(author.name)
        console.log(isAuthor)
        if (isAuthor.length !== 0) {
            console.log("noo")
            throw new HttpException( 'this author is already exists', HttpStatus.FORBIDDEN);
        }
        if (isAuthor.length === 0) {
            console.log("yees")
            let createdAuthor = new this.authorModel(author)
            return createdAuthor.save()
        }     
    }

    public async getAllAuthors(): Promise<AuthorModel[]> {
        return this.authorModel.find().exec();
    }

    public async paging(page): Promise<AuthorModel[]> {
        const perPage = 5;
        const start = (page - 1) * perPage;
        const authorsOnPage = this.authorModel.find().skip(start).limit(perPage)
        return authorsOnPage
    }

    public async getTotal(): Promise<{}> {
        const total = this.authorModel.find().count()
        return total
    }

    public async changeAuthorName(author): Promise<AuthorModel> {
        const isAuthor = this.findAuthorById(author._id)

        if(!isAuthor) {
            throw new HttpException( 'author is undefained', HttpStatus.FORBIDDEN);
        }

        if (isAuthor) {
            return this.authorModel.updateOne({_id: author._id}, author)
        }
    }

    public async deleteAuthor(author): Promise<AuthorModel> {
        const isAuthor = this.findAuthorById(author._id)

        if (!isAuthor) {
            throw new HttpException( 'author is undefained', HttpStatus.FORBIDDEN);
        }

        if (isAuthor) {
            return this.authorModel.remove(author)
        }
    }
}
